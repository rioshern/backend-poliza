package com.ma.scripts.pom.service;

import java.util.Map;

public class Replacements {

    private final String proyectName;

    public Replacements(String proyectName) {
        this.proyectName = proyectName;
    }

    public Map<String, String> getReplacements() {
        return Map.of(
                "<artifactId>template</artifactId>", "<artifactId>" + proyectName + "</artifactId>",
                "<name>template</name>", "<name>" + proyectName + "</name>",
                "<artifactId>template-core</artifactId>" , "<artifactId>" + proyectName + "-core</artifactId>",
                "<artifactId>template-infra</artifactId>" , "<artifactId>" + proyectName + "-infra</artifactId>",
                "<artifactId>template-web</artifactId>", "<artifactId>" + proyectName + "-web</artifactId>",
                "<name>template-web</name>", "<name>" + proyectName + "-web</name>"
        );
    }
    
}
