package com.ma.scripts.pom.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.ma.scripts.shared.ErrorUtils;

public class PomFileRepository {

    private final static String POM_NAME = "pom.xml";
    private final static List<String> SUBMODULES = List.of("core", "infra", "web");

    public List<String> getLines(String path) {
        try {
            return Files.readAllLines(Path.of(path), StandardCharsets.UTF_8);
        } catch (IOException error) {
            ErrorUtils.printError(error);
            return Collections.emptyList();
        }
    }

    public void saveLines(String path, List<String> lines) {
        try {
            Files.write(Path.of(path), lines, StandardCharsets.UTF_8);
        } catch (IOException error) {
            ErrorUtils.printError(error);
        }
    }

    public List<String> getPaths(String rootPath) {
        List<String> paths = new ArrayList<>();
        paths.add(rootPath.concat(File.separator).concat(POM_NAME));
        paths.addAll(getPathsSubmodules(rootPath));
        return  paths;
    }

    private List<String> getPathsSubmodules(String rootPath) {
        return SUBMODULES
                .stream()
                .map(folder -> getPathSubmodule(rootPath, folder))
                .collect(Collectors.toList());
    }

    private String getPathSubmodule(String rootPath, String module) {
        return rootPath
                .concat(File.separator)
                .concat(module)
                .concat(File.separator)
                .concat(POM_NAME);
    }
    
}
