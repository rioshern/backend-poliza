package com.ma.scripts.shared;

import java.io.Console;

public class ErrorUtils {

    private ErrorUtils() {}

    public static void printError(Throwable error) {
        Console console = System.console();
        if (console != null) {
            console.printf("%s%n", error.getMessage());
        }
    }
}
