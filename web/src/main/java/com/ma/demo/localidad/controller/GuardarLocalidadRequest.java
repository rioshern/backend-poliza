package com.ma.demo.localidad.controller;

import javax.validation.constraints.*;

public class GuardarLocalidadRequest {

    @NotNull
    @Size(min = 1, max = 30)
    private String nombre;

    @NotNull
    @Min(1000)
    @Max(9999)
    private Integer codigoPostal;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Override
    public String toString() {
        return "GuardarLocalidadRequest{" +
                "nombre='" + nombre + '\'' +
                ", codigoPostal=" + codigoPostal +
                '}';
    }
}
