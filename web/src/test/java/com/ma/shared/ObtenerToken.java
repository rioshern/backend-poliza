package com.ma.shared;

import static io.restassured.RestAssured.given;

public class ObtenerToken {

    public static String token(String user, String password) {
        String token =
        given()
              .baseUri("https://idm.qamercantilandina.com.ar")
              .formParam("grant_type", "password")
              .formParam("username", user)
              .formParam("password", password)
              .formParam("client_id", "spa-sima-web")
        .when()
              .post("/auth/realms/meran/protocol/openid-connect/token")
        .then()
              .statusCode(200)
        .extract()
              .path("access_token");
        return token;
    }
}
