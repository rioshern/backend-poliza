package com.ma.demo.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CrearLocalidadRequestMother {

    public static Map<String, Serializable> ejemplo() {
        Map<String, Serializable> localidad = new HashMap<>();
        localidad.put("nombre", "Tandil");
        localidad.put("codigoPostal", 4544);
        return localidad;
    }

    public static Map<String, Serializable> ejemploSinNombre() {
        Map<String, Serializable> localidad = new HashMap<>();
        localidad.put("codigoPostal", 4544);
        return localidad;
    }
}
