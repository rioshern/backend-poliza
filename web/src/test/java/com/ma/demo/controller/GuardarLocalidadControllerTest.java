package com.ma.demo.controller;

import com.ma.shared.ContainerBaseTest;
import com.ma.shared.ObtenerToken;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("web")
class GuardarLocalidadControllerTest extends ContainerBaseTest {

    @LocalServerPort
    int port;

    private static String token;

    @BeforeAll
    public static void setup() {
        token = ObtenerToken.token("prod1", "prod1");
    }

    @BeforeEach
    void init() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
    }

    @Test
    void deberia_guardar_localidad() {
        given()
                .contentType(ContentType.JSON)
                .header("Authorization", String.format("Bearer %s", token))
                .body(CrearLocalidadRequestMother.ejemplo())
        .when()
                .put("/localidades/1234")
        .then()
                .statusCode(200);
    }

    @Test
    void deberia_dar_error_al_ingresar_nombre_vacio() {
        given()
                .contentType(ContentType.JSON)
                .header("Authorization", String.format("Bearer %s", token))
                .body(CrearLocalidadRequestMother.ejemploSinNombre())
        .when()
                .put("/localidades/1234")
        .then()
                .statusCode(400)
        .assertThat().body("errors", hasItems(hasEntry("message", "no debe ser nulo")));
    }

}