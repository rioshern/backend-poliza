# Servicios de aplicación

Implementa un caso de uso, sin embargo no debe incluir lógica de negocio. Su responsabilidad es coordinar acciones sobre las clases de dominio, repositorios y bus de eventos.

Las clases de servicio de aplicación se incluyen en el paquete _application_ en el módulo _core_. Se debe marcar con la anotación ```com.ma.shared.domain.Service```.

## Separar casos de uso

Es conveniente crear una clase de servicio por cada caso de uso, por más que tengamos varios casos asociados a un dominio.

```java
@Service
/**
 * Mal
 */
public class ClienteService {

    public void crear(Cliente cliente) {
        ...
    }

    public Optional<Cliente> consultar(String id) {
        ...
    }
}
``` 
*Ejemplo de una implementación no recomendada usando una clase que agrupa varios casos de usos* 

        
```java
/**
 * Bien
 */
@Service
public class CrearCliente {
    public void crear(Cliente cliente) {
        ...
    }
}
```
```java
/**
 * Bien
 */
@Service
public class ConsultarCliente {
    public Optional<Cliente> consultar(String id) {
       ...
    }
}
```
*Ejemplos de casos de usos, cada uno implementado en un servicio diferente.*

## Publicar eventos de dominio

Una de las responsabilidades de un servicio de aplicación es utilizar un bus de eventos para publicar eventos de dominio. 

Los eventos de dominio se generan después de realizar alguna acción sobre un agregado. Para obtener los eventos utilizar el método _pullEvents_ del agregado. Posteriormente utilizar el método _publish_ de la interface _EventBus_ para publicar los eventos.

```java
    public void guardar(LocalidadInder inder, LocalidadNombre nombre, LocalidadCodigoPostal codigoPostal) {
        Localidad localidad = Localidad.guardar(inder, nombre, codigoPostal);
        localidadRepository.guardar(localidad);
        eventBus.publish(localidad.pullEvents());
        logger.info("Localidad guardada: {}", localidad.getInder().getValor());
    }
```
*Ejemplo de un caso de uso que publica eventos de dominio* Ver [GuardarLocalidad.java](../../core/src/main/java/com/ma/demo/localidad/application/GuardarLocalidad.java)