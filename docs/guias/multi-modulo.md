# Proyecto de Maven multimódulo

Este proyecto cuenta con tres módulos principales:

- core
- infra
- web

## Módulo core
Aquí se encuentran todas las clases de dominio que sólo contiene lógica de negocio. Entre los distintos tipos de clases encontramos:

- Agregados
- Entidades
- Value Object
- Eventos de dominio
- Servicios de dominio
- Servicios de aplicación

Es importante tener en cuenta que en este módulo no debe hacerse ninguna referencia a objetos del framework ni clases incluidas en los otros módulos. 

## Módulo infra
En este módulo se encuentran la implementación de los repositorios y otros objetos de infraestructura como pueden ser un bus de eventos. 

Los tipos de clases que podemos encontrar son:

- Entidades JPA
- Repositorios de base de datos
- Clientes de servicios web


## Modulo web
Aquí se escriben principalmente las clases que correspondan a la capa web como controladores, objetos DTOs, y la configuración de seguridad.

También se incluyen todos los suscriptores a eventos de dominio sean tanto externos como internos. 

En este módulo se encuentra la clase principal y el archivo de configuración de Spring Boot.
