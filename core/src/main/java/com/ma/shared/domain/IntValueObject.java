package com.ma.shared.domain;

import java.util.Objects;

public class IntValueObject {

    private final Integer valor;

    public IntValueObject(Integer valor) {
        this.valor = valor;
    }

    public Integer getValor() {
        return valor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IntValueObject)) return false;
        IntValueObject that = (IntValueObject) o;
        return Objects.equals(valor, that.valor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(valor);
    }
}
