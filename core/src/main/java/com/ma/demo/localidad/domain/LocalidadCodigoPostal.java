package com.ma.demo.localidad.domain;

import com.ma.shared.domain.IntValueObject;

public class LocalidadCodigoPostal extends IntValueObject {

    public LocalidadCodigoPostal(Integer valor) {
        super(valor);
        validarCodigoPostal(valor);
    }

    private void validarCodigoPostal(Integer valor) {
        if (valor == null || valor < 0) {
            throw new IllegalArgumentException("Código postal no válido");
        }
    }

    @Override
    public String toString() {
        return "LocalidadCodigoPostal{valor = " + getValor() + "}";
    }
}
