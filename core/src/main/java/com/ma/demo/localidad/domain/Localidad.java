package com.ma.demo.localidad.domain;

import com.ma.shared.domain.AggregateRoot;

import java.util.Objects;

public class Localidad extends AggregateRoot {

    private final LocalidadInder inder;
    private final LocalidadNombre nombre;
    private final LocalidadCodigoPostal codigoPostal;

    public Localidad(LocalidadInder inder, LocalidadNombre nombre, LocalidadCodigoPostal codigoPostal) {
        this.inder = Objects.requireNonNull(inder, "inder no puede ser nulo");
        this.nombre = Objects.requireNonNull(nombre, "nombre no puede ser nulo");
        this.codigoPostal = Objects.requireNonNull(codigoPostal, "codigo postal no puede ser nulo");
    }

    public static Localidad guardar(LocalidadInder inder, LocalidadNombre nombre, LocalidadCodigoPostal codigoPostal) {
        Localidad localidad = new Localidad(inder, nombre, codigoPostal);
        localidad.push(new LocalidadGuardada(inder.getValor(), nombre.getValor(), codigoPostal.getValor()));
        return  localidad;
    }

    public LocalidadInder getInder() {
        return inder;
    }

    public LocalidadNombre getNombre() {
        return nombre;
    }

    public LocalidadCodigoPostal getCodigoPostal() {
        return codigoPostal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Localidad)) return false;
        Localidad localidad = (Localidad) o;
        return Objects.equals(inder, localidad.inder) && Objects.equals(nombre, localidad.nombre) &&
               Objects.equals(codigoPostal, localidad.codigoPostal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inder, nombre, codigoPostal);
    }

    @Override
    public String toString() {
        return "Localidad{" +
                "inder=" + inder +
                ", nombre=" + nombre +
                ", codigoPostal=" + codigoPostal +
                '}';
    }
}
