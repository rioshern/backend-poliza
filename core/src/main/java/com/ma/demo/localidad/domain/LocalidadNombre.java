package com.ma.demo.localidad.domain;

import com.ma.shared.domain.StringValueObject;

public class LocalidadNombre extends StringValueObject {

    public LocalidadNombre(String valor) {
        super(valor);
        validarNombre(valor);
    }

    private void validarNombre(String valor) {
        if (valor == null || valor.length() == 0) {
            throw new IllegalArgumentException("Nombre no válido");
        }
    }

    @Override
    public String toString() {
        return "LocalidadNombre{valor=" + getValor() + "}";
    }
}
