package com.ma.demo.georeferencia.domain;

import java.util.Objects;

public class GeoreferenciaPunto {

    private final Double latitud;
    private final Double longitud;

    public GeoreferenciaPunto(Double latitud, Double longitud) {
        validar(latitud, longitud);
        this.latitud = latitud;
        this.longitud = longitud;
    }

    private void validar(Double latitud, Double longitud) {
        if (latitud < -90.0  || latitud > 90.00 || longitud < -90.0 || longitud > 90.0) {
            throw new IllegalArgumentException("Datos de latitud y longitud no válidos");
        }
    }

    public Double getLatitud() {
        return latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    @Override
    public String toString() {
        return "GeoreferenciaPunto{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GeoreferenciaPunto)) return false;
        GeoreferenciaPunto punto = (GeoreferenciaPunto) o;
        return Objects.equals(latitud, punto.latitud) && Objects.equals(longitud, punto.longitud);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitud, longitud);
    }
}
