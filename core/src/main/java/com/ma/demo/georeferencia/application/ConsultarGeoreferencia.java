package com.ma.demo.georeferencia.application;

import com.ma.demo.georeferencia.domain.Georeferencia;
import com.ma.demo.georeferencia.domain.GeoreferenciaRepository;
import com.ma.shared.domain.Service;

import java.util.Optional;

@Service
public class ConsultarGeoreferencia {

    private final GeoreferenciaRepository repository;

    public ConsultarGeoreferencia(GeoreferenciaRepository repository) {
        this.repository = repository;
    }

    public Optional<Georeferencia> consultar(String nombre) {
        return repository.buscarPorNombre(nombre);
    }
}
