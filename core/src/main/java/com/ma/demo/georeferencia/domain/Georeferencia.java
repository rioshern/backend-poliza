package com.ma.demo.georeferencia.domain;

public class Georeferencia {
    private final GeoreferenciaPunto punto;
    private final GeoreferenciaNombre nombre;

    public Georeferencia(GeoreferenciaPunto punto, GeoreferenciaNombre nombre) {
        this.punto = punto;
        this.nombre = nombre;
    }

    public GeoreferenciaPunto getPunto() {
        return punto;
    }

    public GeoreferenciaNombre getNombre() {
        return nombre;
    }

    @Override
    public String toString() {
        return "Georeferencia{" +
                "punto=" + punto +
                ", nombre=" + nombre +
                '}';
    }
}
