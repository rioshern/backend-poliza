package com.ma.demo.georeferencia.application;

import com.ma.demo.georeferencia.domain.Georeferencia;
import com.ma.demo.georeferencia.domain.GeoreferenciaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static org.mockito.Mockito.*;

class ConsultarGeoreferenciaTest {

    private GeoreferenciaRepository georeferenciaRepository;
    private ConsultarGeoreferencia consultarGeoreferencia;

    @BeforeEach
    void setup() {
        georeferenciaRepository = mock(GeoreferenciaRepository.class);
        consultarGeoreferencia = new ConsultarGeoreferencia(georeferenciaRepository);
    }

    @Test
    void deberia_consultar_georeferencia() {
        Georeferencia ejemplo = GeoreferenciaMother.getEjemplo();

        when(georeferenciaRepository
                .buscarPorNombre("Tandil"))
                .thenReturn(Optional.of(GeoreferenciaMother.getEjemplo()));

        Optional<Georeferencia> georeferencia = consultarGeoreferencia.consultar("Tandil");
        assertThat(georeferencia.isPresent(), is(true));
        Georeferencia datos = georeferencia.get();
        assertThat(datos.getNombre(), is(ejemplo.getNombre()));
        assertThat(datos.getPunto(), is(ejemplo.getPunto()));
    }

}