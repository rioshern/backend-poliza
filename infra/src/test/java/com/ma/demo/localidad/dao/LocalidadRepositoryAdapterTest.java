package com.ma.demo.localidad.dao;

import com.ma.TestConfig;
import com.ma.demo.localidad.domain.*;
import com.ma.shared.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = TestConfig.class)
class LocalidadRepositoryAdapterTest extends IntegrationTest {

    @Autowired
    private LocalidadRepository localidadRepository;

    @Test
    void deberiaInsertarLocalidad() {
        Localidad localidad = new Localidad(new LocalidadInder(1234), new LocalidadNombre("Tandil"),
                              new LocalidadCodigoPostal(7000));
        localidadRepository.guardar(localidad);
    }
}