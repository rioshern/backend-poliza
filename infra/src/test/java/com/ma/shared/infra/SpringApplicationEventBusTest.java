package com.ma.shared.infra;

import com.ma.TestConfig;
import com.ma.shared.IntegrationTest;
import com.ma.shared.domain.DomainEvent;
import com.ma.shared.domain.EventBus;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfig.class)
class SpringApplicationEventBusTest extends IntegrationTest {

    @Autowired
    EventBus eventBus;

    @Autowired
    SomeEventListener listener;

    @Test
    void deberia_enviar_evento() {
        List<DomainEvent> events = Collections.singletonList(new SomeEvent());
        eventBus.publish(events);
        assertTrue(listener.isActivated());
    }

    @Component
    public static class SomeEventListener {
        private AtomicBoolean activated = new AtomicBoolean(false);
        private Logger logger = LoggerFactory.getLogger(SomeEventListener.class);

        @EventListener(SomeEvent.class)
        public void listen(SomeEvent event){
            logger.debug("capture event: {}" , event.eventName());
            activated.set("test.event".equals(event.eventName()));
        }

        public boolean isActivated() {
            return activated.get();
        }
    }

    public static class SomeEvent extends DomainEvent {

        protected SomeEvent() {
            super("agregate-id");
        }

        @Override
        public String eventName() {
            return "test.event";
        }
    }
}