package com.ma.shared.infra;

import com.ma.shared.domain.IntegrationEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class RabbitMQEventPublisher {

    private static final Logger logger = LoggerFactory.getLogger(RabbitMQEventPublisher.class);

    private final RabbitTemplate rabbitTemplate;
    private final SQLEventPublisher failoverPublisher;

    public RabbitMQEventPublisher(RabbitTemplate rabbitTemplate, SQLEventPublisher failoverPublisher) {
        this.rabbitTemplate = rabbitTemplate;
        this.failoverPublisher = failoverPublisher;
    }

    @EventListener(IntegrationEvent.class)
    public void onIntegrationEvent(IntegrationEvent event) {
        try {
            publish(event);
        } catch (Exception error) {
            logger.error("Error sending event to Rabbit {}", error.getMessage());
            failoverPublisher.publish(event);
        }
    }

    private void publish(IntegrationEvent event) {
        Map<String, Object> eventData =IntegrationEventSerializer.serialize(event);
        logger.info("Enviando evento a Rabbit {}", eventData);
        rabbitTemplate.convertAndSend(event.getExchangeName(), event.eventName(), eventData);
    }

}
