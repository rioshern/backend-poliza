package com.ma.shared.config.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

@Configuration
public class RabbitMQDeadLetter {

    Logger logger = LoggerFactory.getLogger(RabbitMQDeadLetter.class);

    private final RabbitProperties rabbitProperties;

    public RabbitMQDeadLetter(RabbitProperties rabbitProperties) {
        this.rabbitProperties = rabbitProperties;
    }

    public List<Declarable> declarables() {
        List<Declarable> declarables = new ArrayList<>();
        BindingProperty deadLetterConfig = rabbitProperties.getDeadLetter();
        if (deadLetterConfig != null) {
            addDeadLetterExchange(declarables, deadLetterConfig);
            addDeadLetterQueue(declarables, deadLetterConfig);
            addDeadLetterBinding(declarables, deadLetterConfig);
        } else {
            logger.info("DeadLetter is no configured");
        }
        return declarables;
    }

    private void addDeadLetterExchange(Collection<Declarable> declarables, BindingProperty deadLetterConfig) {
        String exchangeName = deadLetterConfig.getExchange();
        if (exchangeName != null) {
            TopicExchange deadLetterExchange = new TopicExchange(exchangeName);
            logger.debug("Add dead-letter exchange {} " , deadLetterExchange);
            declarables.add(deadLetterExchange);
        } else {
            logger.debug("No DeadLetter exchange is added");
        }
    }

    private void addDeadLetterQueue(Collection<Declarable> declarables, BindingProperty deadLetterConfig) {
        String queueName = deadLetterConfig.getQueue();
        if (queueName != null) {
            Queue deadLetterQueue = QueueBuilder.durable(queueName).build();
            logger.debug("Add dead-letter queue {} " , deadLetterQueue);
            declarables.add(deadLetterQueue);
        } else {
            logger.debug("No DeadLetter queue is added");
        }
    }

    private void addDeadLetterBinding(Collection<Declarable> declarables, BindingProperty deadLetterConfig) {
        String deadLetterExchange = deadLetterConfig.getExchange();
        String deadLetterQueue = deadLetterConfig.getQueue();
        String deadLetterRoutingKey = deadLetterConfig.getRoutingKey();
        if (deadLetterExchange != null && deadLetterQueue != null && deadLetterRoutingKey != null) {
            Binding binding = new Binding(deadLetterQueue, QUEUE, deadLetterExchange, deadLetterRoutingKey, null);
            logger.debug("Add dead-letter routing key {} " , deadLetterRoutingKey);
            declarables.add(binding);
        } else {
            logger.debug("No DeadLetter binding is added");
        }
    }

}
