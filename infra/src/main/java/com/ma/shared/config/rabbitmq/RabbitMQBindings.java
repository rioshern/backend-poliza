package com.ma.shared.config.rabbitmq;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.amqp.core.Binding;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

@Configuration
public class RabbitMQBindings {

    private final RabbitProperties rabbitProperties;

    public RabbitMQBindings(RabbitProperties rabbitProperties) {
        this.rabbitProperties = rabbitProperties;
    }

    public List<Binding> bindings() {
        List<BindingProperty> listBindings = rabbitProperties.getBindings();
        if (listBindings == null) {
            return Collections.emptyList();
        }
        return listBindings.stream()
                .filter(BindingProperty::isValid)
                .map(this::binding)
                .collect(Collectors.toList());
    }

    public Binding binding(BindingProperty bindingProperty) {
        return new Binding(bindingProperty.getQueue(), QUEUE,
                           bindingProperty.getExchange(), 
                           bindingProperty.getRoutingKey(), null);
    }
    
}
