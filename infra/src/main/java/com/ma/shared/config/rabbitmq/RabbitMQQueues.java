package com.ma.shared.config.rabbitmq;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQQueues {

    private final RabbitProperties rabbitProperties;

    public RabbitMQQueues(RabbitProperties rabbitProperties) {
        this.rabbitProperties = rabbitProperties;
    }

    public List<Queue> queues() {
        List<QueueProperty> listQueuesProperties = rabbitProperties.getQueues();
        QueueProperty defaultProperties = rabbitProperties.getDefaultQueue();
        BindingProperty deadLetterProperty = rabbitProperties.getDeadLetter();

        if (listQueuesProperties == null) {
            return Collections.emptyList();
        }
        return listQueuesProperties.stream()
                         .map(properties -> properties.getPropertiesOrDefaultValue(defaultProperties))
                         .map(properties -> createQueue(properties, deadLetterProperty))
                         .collect(Collectors.toList());
    }

    private Queue createQueue(QueueProperty queueProperty, BindingProperty deadLetterProperty) {
        Queue queue = new Queue(queueProperty.getName(), queueProperty.getDurable(), queueProperty.getExclusive(), queueProperty.getAutoDelete(), null);
        return addDeadLetter(queue, deadLetterProperty);
    }

    private Queue addDeadLetter(Queue queue, BindingProperty deadLetterProperty) {
        if (deadLetterProperty == null) {
            return queue;
        }
        addDeadLetterExchange(queue, deadLetterProperty.getExchange());
        addDeadLetterRoutingKey(queue, deadLetterProperty.getRoutingKey());
        return queue;
    }

    private void addDeadLetterExchange(Queue queue, String deadLetterExchange) {
        if (deadLetterExchange == null) {
            return;
        }
        queue.addArgument("x-dead-letter-exchange", deadLetterExchange);
    }

    private void addDeadLetterRoutingKey(Queue queue, String deadLetterRoutingKey) {
        if (deadLetterRoutingKey == null) {
            return;
        }
        queue.addArgument("x-dead-letter-routing-key", deadLetterRoutingKey);
    }
    
}
