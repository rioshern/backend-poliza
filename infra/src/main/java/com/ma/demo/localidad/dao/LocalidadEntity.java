package com.ma.demo.localidad.dao;

import com.ma.demo.localidad.domain.Localidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "localidad")
public class LocalidadEntity {

    @Id
    private Integer inder;
    private String nombre;
    @Column(name = "codigo_postal")
    private Integer codigoPostal;

    public LocalidadEntity() {}

    public LocalidadEntity(Integer inder, String nombre, Integer codigoPostal) {
        this.inder = inder;
        this.nombre = nombre;
        this.codigoPostal = codigoPostal;
    }

    public static LocalidadEntity fromDomain(Localidad localidad) {
        return new LocalidadEntity(localidad.getInder().getValor(), localidad.getNombre().getValor(),
                                   localidad.getCodigoPostal().getValor());
    }

    public Integer getInder() {
        return inder;
    }

    public void setInder(Integer inder) {
        this.inder = inder;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(Integer codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
}
