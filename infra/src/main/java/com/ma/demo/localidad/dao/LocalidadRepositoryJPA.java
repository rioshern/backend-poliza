package com.ma.demo.localidad.dao;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalidadRepositoryJPA extends JpaRepository<LocalidadEntity, Integer> {
}
