package com.ma.demo.georeferencia.adapter;

import com.ma.demo.georeferencia.client.CentroideResponse;
import com.ma.demo.georeferencia.client.GeoreferenciaClient;
import com.ma.demo.georeferencia.client.GeoreferenciaResponse;
import com.ma.demo.georeferencia.client.LocalidadResponse;
import com.ma.demo.georeferencia.domain.Georeferencia;
import com.ma.demo.georeferencia.domain.GeoreferenciaNombre;
import com.ma.demo.georeferencia.domain.GeoreferenciaPunto;
import com.ma.demo.georeferencia.domain.GeoreferenciaRepository;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class GeoreferenciaAdapter implements GeoreferenciaRepository {

    private final GeoreferenciaClient georeferenciaClient;

    public GeoreferenciaAdapter(GeoreferenciaClient georeferenciaClient) {
        this.georeferenciaClient = georeferenciaClient;
    }

    @Override
    @CircuitBreaker(name = "demo")
    @Bulkhead(name = "demo")
    public Optional<Georeferencia> buscarPorNombre(String nombre) {
        GeoreferenciaResponse response = georeferenciaClient.getGeoreferencia(nombre);
        if (emptyReponse(response)) {
            return Optional.empty();
        }
        return Optional.of(toDomain(response.getLocalidades().get(0)));
    }

    private boolean emptyReponse(GeoreferenciaResponse response) {
        return response == null || response.getLocalidades() == null || response.getLocalidades().isEmpty();
    }

    private Georeferencia toDomain(LocalidadResponse localidadResponse) {
        CentroideResponse centroide = localidadResponse.getCentroide();
        GeoreferenciaNombre nombre = new GeoreferenciaNombre(localidadResponse.getNombre());
        GeoreferenciaPunto punto = new GeoreferenciaPunto(centroide.getLat(), centroide.getLon());
        return new Georeferencia(punto, nombre);
    }
}
