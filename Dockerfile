FROM openjdk:11-jdk-slim as builder
WORKDIR application
ARG JAR_FILE=web/target/*.jar
COPY ${JAR_FILE} application.jar
RUN jar -xf application.jar
RUN mkdir -p dependency && (cd dependency; jar -xf ../application.jar)

FROM openjdk:11-jre-slim
VOLUME /tmp
ARG DEPENDENCY=/application/dependency
COPY --from=builder ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=builder ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=builder ${DEPENDENCY}/BOOT-INF/classes /app
RUN groupadd -r spring && useradd --no-log-init -r -g spring spring
USER spring
ENTRYPOINT ["java","-Dspring.profiles.active=cloud","-cp","app:app/lib/*","com.ma.WebApp"]

